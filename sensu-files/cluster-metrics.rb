#!/usr/bin/env ruby
require 'rubygems'
require 'net/http'
require 'json'
require 'socket'

cluster_name = "qbol_default"
hostname     = Socket.gethostname

metrics = 	[	
		'load_one',
                'proc_total',
                'cpu_idle',
                'cpu_wio',
                'cpu_steal',
                'mem_free',
                'mem_cached',
		'disk_free',
                'bytes_in',
                'bytes_out',

                'jvm.metrics.threadsBlocked',
                'jvm.metrics.threadsWaiting',
                'jvm.metrics.threadsTimedWaiting',
		'jvm.metrics.threadsNew',
                'jvm.metrics.gcCount',
                'jvm.metrics.gcTimeMillis',
		'jvm.metrics.memHeapUsedM',
                'jvm.metrics.memNonHeapUsedM',
                'jvm.metrics.logError',
                'jvm.metrics.logFatal',

		'mapred.jobtracker.map_slots',
		'mapred.jobtracker.occupied_map_slots',
		'mapred.jobtracker.reduce_slots',
		'mapred.jobtracker.occupied_reduce_slots',
		'mapred.jobtracker.jobs_submitted',
		'mapred.jobtracker.reduces_completed',
		'mapred.jobtracker.jobs_running',
		'mapred.jobtracker.jobs_completed',
		'mapred.jobtracker.num_tasks_in_memory',
		'mapred.jobtracker.maps_launched',
		'mapred.jobtracker.running_maps',
		'mapred.jobtracker.waiting_maps',
		'mapred.jobtracker.maps_completed',
		'mapred.jobtracker.reduces_launched',
		'mapred.jobtracker.running_reduces',
		'mapred.jobtracker.waiting_reduces',
                'mapred.jobtracker.killed_tasks_reduce_time',
                'mapred.jobtracker.failed_tasks_map_time',
                'mapred.jobtracker.maps_killed',
                'mapred.jobtracker.reduces_killed',
                'mapred.jobtracker.trackers',
                'mapred.jobtracker.trackers_dead',
                'mapred.jobtracker.trackers_decommissioned',

                'mapred.fairscheduler.num_running_jobs',
                'mapred.fairscheduler.total_max_maps',
                'mapred.fairscheduler.num_starved_jobs'
		]

metrics.each do |metric|
        res = Net::HTTP.start('localhost', 80) do |http|
                http.get("/ganglia/graph.php?c=#{cluster_name}&h=master&r=1hour&m=#{metric}&json=1")
        end
        result = JSON.parse(res.body)
        result.each do |x|
                dp = x['datapoints'][-3]
                puts "#{hostname}.#{metric} #{dp[0]} #{dp[1]}"
        end
end
